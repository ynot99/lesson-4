from time import time
from datetime import datetime

from flask.wrappers import Request


class RouteTimer:
    def __init__(self, logs_filename: str, request: Request):
        self.logs_filename = logs_filename
        self.request = request

    def __enter__(self):
        self.time_start = time()

    def __exit__(self, exc_type, exc_val, exc_tb):
        done_by_ms = int((time() - self.time_start) * 1000)
        path = self.request.path
        method = self.request.method
        date_now = datetime.now().strftime("%d/%m/%Y %H:%M:%S")
        uncaught_error = exc_val if exc_type is not None else ""
        with open(self.logs_filename, "a") as file:
            file.write(
                f'[{date_now}] Request: "{method} {path}" - Response time: {done_by_ms} ms {uncaught_error}\n'
            )
