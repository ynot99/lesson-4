import requests

from flask import Flask, render_template, Response, request

from context_managers import RouteTimer

app = Flask(__name__)

LOGS_FILE = "logs.txt"


@app.route("/astro/list")
def get_astro_list():
    with RouteTimer(LOGS_FILE, request):
        astros_json = requests.get("http://api.open-notify.org/astros.json").json()
        return render_template("astros.j2", astros=astros_json["people"])


@app.route("/astro/craft/<craft_name>")
def get_astro_list_by_craft(craft_name):
    with RouteTimer(LOGS_FILE, request):
        astros_json = requests.get("http://api.open-notify.org/astros.json").json()
        return render_template(
            "astros.j2",
            astros=filter(lambda astro: astro["craft"] == craft_name, astros_json["people"]),
        )


@app.route("/file/<file_name>")
def get_file_content(file_name):
    with RouteTimer(LOGS_FILE, request):
        file_content: str
        try:
            with open(file_name, "r") as file:
                file_content = file.read()
        except FileNotFoundError:
            return Response(f"Error, file {file_name} not found")

        return render_template("file.j2", file_name=file_name, file_content=file_content)


@app.route("/logs")
def get_logs():
    logs: str
    with open(LOGS_FILE, "r+") as file:
        logs = file.read()

    return render_template("file.j2", file_name=LOGS_FILE, file_content=logs)


if __name__ == "__main__":
    app.run(debug=True)
